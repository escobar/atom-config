## 0.4.5
* Support for haskell-ghc-mod 0.8.0

## 0.4.4
* Bail on completion if no backend

## 0.4.3
* Deactivation fix

## 0.4.2
* Set inclusionPriority=0

## 0.4.1
* atom-backend-helper version bump

## 0.4.0
* Use haskell-completion-backend service
* Add message on multiple backends and none selected
* Possible undefined symbol module
* Remove dep on underscore-plus
* Specify atom version according to docs
* Specify package versions
* Add info on haskell-completion-backend service
* Use fuzzaldrin
* Added pragma words, general overhaul
* Cleanup, array scopes, support for exportsScope

## 0.3.1
* Fix deprecated ac+ blacklist option

## 0.3.0
* Updated to work with newest haskell-ghc-mod
* Hole completions no longer depend on Hoogle
* Autocomplete+ API 2.0
* Changed EditorController to BufferController

## 0.2.1
* BUGFIX: activation problems

## 0.2.0
* Migrate to new json-based service provider
* Move out provider code to sep. file
* Bump atom version

## 0.1.2
* Fix typo
* A little refactoring

## 0.1.1
* Use atom.services.consume once and pass reference around

## 0.1.0 - First Release
* Initial release
